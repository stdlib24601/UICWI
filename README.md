# UI coursework

== Build instructions

Type

$ qmake

Then

$ make

$ ls -lt

will list all files contained in this directory, ordered by modification time.
note the newly created file "UICWI".

== Usage

Run the program with

$ ./UICWI
